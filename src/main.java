import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class main{
	
	public static void main(String[] args){
		String password = "practicas";
		SecureRandom random = new SecureRandom();
		byte[] salt = new byte[16];
		random.nextBytes(salt);
		KeySpec spec = new PBEKeySpec(password.toCharArray(),salt,0xffff,128);
		try {
			SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			byte[] hash = factory.generateSecret(spec).getEncoded();
			String passwordHash = Base64.getEncoder().encodeToString(hash) + 
								  Base64.getEncoder().encodeToString(salt);
			System.out.println(passwordHash);
		}catch(NoSuchAlgorithmException e) {
			e.printStackTrace();
		}catch(InvalidKeySpecException e) {
			e.printStackTrace();
		}
	}
	

}
