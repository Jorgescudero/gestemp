

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/gi")
public class ingenierosServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ingenierosServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		HTML5.comienzo(out,"Gestión de Ingenieros");
		out.println("<form method=\"get\"action=\"/gi\">");
		out.println("<p><label for=\"dep\">Departamento</label>");
		out.println("<select name=\"dep\">");
		agregarDepartamento(out);
		out.println("</select>");
		out.println("</form>");
		HTML5.fin(out);
		
	}

	
	protected void agregarDepartamento(PrintWriter out) {
		try {
			Connection c = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/Gestión de Empleados","jorge","practicas");
			Statement sql = c.createStatement();
			ResultSet rs = sql.executeQuery("select nombre from departamentos");
			while(rs.next()) {
				out.printf("<option>%s</option>",rs.getString("nombre"));
			}
			rs.close();
			c.close();
				
		}catch(SQLException e){
			
		}
	}

}
