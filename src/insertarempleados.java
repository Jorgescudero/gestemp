

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

@WebServlet("/ie")
public class insertarempleados extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	private DataSource dataSource;
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		try {
			InitialContext context =new InitialContext();
			dataSource = (DataSource) context.lookup("java:comp/env/jdbc/ge");
			if(dataSource==null) {
				throw new ServletException("DataSource desconocido");
			}
		} catch (NamingException e) {
			Logger.getLogger(insertarempleados.class.getName()).log(Level.SEVERE,null,e);
		}
	}
    public insertarempleados() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		HTML5.comienzo(out,"Insertar empleados");
		out.println("<form method=\"GET\"action=\"ie\" style=\"background-color:white; width:350px;margin-left:25%; margin-bottom: 10px; border:solid black; border-radius:10px;\">");
		out.println("NSS: <input type=\"number\" name=\"nss\" style=\"border:none; border-bottom: solid black; height:15px; margin-bottom: 10px; margin-left:5px;\"><br>");
		out.println("Nombre: <input type=\"text\" name=\"nombre\" style=\"border:none; border-bottom: solid black; height:15px; margin-bottom: 10px; margin-left:5px;\"><br>");
		out.println("Salario: <input type=\"number\" name=\"salario\" style=\"border:none; border-bottom: solid black; height:15px; margin-bottom: 10px; margin-left:5px;\"><br>");
		out.println("<p style=\"margin-left:5px;\"><label for=\"dep\">Departamento</label>");
		out.println("<select name=\"dep\">");
		agregarDepartamento(out);
		out.println("</select><br>");
		out.println("Especialidad: <input type=\"text\" name=\"especialidad\" style=\"border:none; border-bottom: solid black; height:15px; margin-bottom: 10px; margin-top:10px; margin-left:5px;\"><br>");
		out.println("<input type=\"submit\" name=\"insertar\" onclick=\"");
		insertarusuario(out,request);
		out.println("\"></form>");
		HTML5.fin(out);
		
	}

	private void insertarusuario(PrintWriter out,HttpServletRequest request){
		try {
		Connection c = dataSource.getConnection();
		String usu = request.getParameter("nombre");
		String nss = request.getParameter("nss");
		String salario = request.getParameter("salario");
		String departamento = request.getParameter("dep");
		String espe = request.getParameter("especialidad");
		PreparedStatement sql = c.prepareStatement("insert into empleados values (?,?,?)");
		sql.setString(1, nss);
		sql.setString(2, usu);
		sql.setString(3, salario);
		
		sql.executeUpdate();
		
		PreparedStatement sql1 = c.prepareStatement("insert into ingenieros values (?,?)");
		sql1.setString(1, nss);
		sql1.setString(2, departamento);
		
		sql1.executeUpdate();
		
		PreparedStatement sql2 = c.prepareStatement("insert into especialidades values (?,?)");
		sql2.setString(1,nss);
		sql2.setString(2, espe);
		
		sql2.executeUpdate();
		
		out.println("Usuario registrado");
		c.close();
		}catch(SQLException e){
			
		}
		
	}

	protected void agregarDepartamento(PrintWriter out) {
		try {
			Connection c = dataSource.getConnection();
			Statement sql = c.createStatement();
			ResultSet rs = sql.executeQuery("select nombre from departamentos");
			while(rs.next()) {
				out.printf("<option>%s</option>",rs.getString("nombre"));
			}
			rs.close();
			c.close();
				
		}catch(SQLException e){
		}
	}

}
